#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp1


class shortener (webapp1.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    urls = {"/":""}

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        metodo,recurso,cuerpo = parsedRequest

        if metodo == "POST":

            if (cuerpo.split('=')[1][0]) == "&" or (cuerpo.split('=')[2]== ""):
                httpCode = "404 Error"
                htmlBody = "<html><body></p><p>Error 404</p></form></body></html>"

            else:
                urlclave=cuerpo.split("=")[2].replace('%2F', '/')
                self.urls[urlclave] = cuerpo.split("=")[1].split('&')[0].replace('%3A', ':').replace('%2F', '/')

                if (self.urls[urlclave][0:8] != "https://") and (self.urls[urlclave][0:7] != "http://"):

                    urlnueva= "https://" + self.urls[urlclave]
                    self.urls[urlclave]= urlnueva

                    httpCode = "200 OK"
                    htmlBody = "<html><body>" \
                               "<p>Url acortada: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/",'', 1).replace("'", '', 2) + \
                               "</p>" \
                               "<p>Url original: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'","") + \
                               "</p>" \
                               "<p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''/>" \
                               "</p></p>" \
                               "<p>URL short: <input type = 'text' name = 'nombre' value = ''/>" \
                               "</p>" \
                               "<p><input type = 'submit' value = 'Enviar' size = '40'>" \
                               "</p>" \
                               "</form>" \
                               "<p> Enlaces: </p>" \
                               "<br><a href='" + self.urls[urlclave] + "'>'" + urlclave + "'</a><br>" \
                               "<br><a href='" + self.urls[urlclave] + "'>'" + self.urls[urlclave] + "'</a><br>" \
                               "</body></html>"
                else:
                    httpCode = "200 OK"
                    htmlBody = "<html><body>" \
                               "<p>Url acortada: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/",'', 1).replace("'", '', 2) + \
                               "</p>" \
                               "<p>Url original: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'","") + \
                               "</p>" \
                               "<p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''/>" \
                               "</p></p>" \
                               "<p>URL short: <input type = 'text' name = 'nombre' value = ''/>" \
                               "</p>" \
                               "<p><input type = 'submit' value = 'Enviar' size = '40'>" \
                               "</p>" \
                               "</form>" \
                               "<p> Enlaces: </p>" \
                               "<br><a href='" + self.urls[urlclave] + "'>'" + urlclave + "'</a><br>" \
                               "<br><a href='" + self.urls[urlclave] + "'>'" + self.urls[urlclave] + "'</a><br>" \
                               "</body></html>"

            return (httpCode,htmlBody)

        if metodo == "GET":
            if recurso in self.urls.keys() and recurso != "/":
                httpCode = "301 Redirect"
                htmlBody = "<html><body>" \
                           "<p>Url acortada: " + str(self.urls.keys()).split('[')[1].split(']')[0].replace(',', ' ').replace("/",'', 1).replace("'", '', 2) + \
                           "</p>" \
                           "<p>Url original: " + str(self.urls.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'","") + \
                           "</p>" \
                           "<p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''/>" \
                           "</p></p>" \
                           "<p>URL short: <input type = 'text' name = 'nombre' value = ''/>" \
                           "</p>" \
                           "<p><input type = 'submit' value = 'Enviar' size = '40'>" \
                           "</p></form>"\
                           "<meta http-equiv = 'Refresh' content = '3; url=" +  self.urls[recurso] + "'>" \
                           "</body></html>"

            elif recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>" \
                           "<p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''/>" \
                           "</p></p>" \
                           "<p>URL short: <input type = 'text' name = 'nombre' value = ''/>" \
                           "</p>" \
                           "<p><input type = 'submit' value = 'Enviar' size = '40'>" \
                           "</p></form></body></html>"


            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>" \
                           "<p><form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''/>" \
                           "</p></p>" \
                           "<p>URL short: <input type = 'text' name = 'nombre' value = ''/>" \
                           "</p>" \
                           "<p><input type = 'submit' value = 'Enviar' size = '40'>" \
                           "</p></form></body></html>"

            return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)